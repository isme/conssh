# frozen_string_literal: true

require 'aws-sdk-ec2'

module Conssh
  class Inventory
    def initialize(args = {})
      args[:client] ||= Aws::EC2::Client.new
      @ec2 = Aws::EC2::Resource.new(:client => args[:client])
    end

    def hosts
      @ec2.instances.to_a
    end
  end
end
