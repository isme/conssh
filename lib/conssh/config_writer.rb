# frozen_string_literal: true

module Conssh
  class ConfigWriter
    def initialize(args = {})
      @out = args[:out] || File.new('host_config', 'w+')
    end

    def render(inst)
      first, *rest = *Array(inst)
      render_one(first) if first
      rest.each do |i|
        @out.puts
        render_one(i)
      end
    end

    private def render_one(inst)
      @out.puts("Host #{inst.id}")
      @out.puts("HostName #{inst.public_ip_address}")
    end
  end
end
