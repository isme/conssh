# frozen_string_literal: true

require_relative 'inventory'
require_relative 'config_writer'

module Conssh
  class App
    def generate_config
      ConfigWriter.new(:out => STDOUT).render(Inventory.new.hosts)
    end
  end
end
