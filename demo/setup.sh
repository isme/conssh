#!/bin/bash

# A fresh Ubuntu image doesn't have a package list, so we need to update it
# first before any of the install commands will work.
apt-get update

# The image doesn't have a bunch of things that you will expect to be there when
# you setting up and playing around. I assume you know what these are, let's
# just get them installed!
apt-get install --yes vim less man wget git

# The image doesn't have an SSH client either, so let's install that. We will
# also quickly set up the usual files with the right permissions so that ssh
# won't complain.
apt-get install --yes openssh-client
mkdir ~/.ssh/
touch ~/.ssh/{known_hosts,config,id_rsa,id_rsa.pub}
chmod --recursive 0600 ~/.ssh/

# Installing AWS CLI is not strictly necessary, but it is the easiest way to
# configure the AWS profiles.
apt-get install --yes awscli

# Install the best autocomplete tool.
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all

# The config generator is a Ruby app, so we need to install it.
# The app is not packaged as a gem but installed from source, so we also have to use bundler
# to install the other Ruby dependencies.
apt-get install --yes ruby
gem install bundler

# Install the config generator.
git clone https://isme@bitbucket.org/isme/ec2-ssh-config-generator.git
cd ec2-ssh-config-generator
bundle install
