#/bin/bash

while : ; do
  read -p "AWS Profile Name: " profile
  if [[ "${profile}" = "" ]]; then
    break
  fi
  aws configure --profile "${profile}"
done

read -p "Press a key to edit SSH private key."
vim ~/.ssh/id_rsa

read -p "Press a key to edit SSH public key."
vim ~/.ssh/id_rsa.pub

read -p "Press a key to edit host config."
echo "
Host <%= profile %>_<%= tags['Name'] %>_<%= instance_id %>
  Hostname <%= public_dns_name %>
" > ~/.ssh/ec2_host.erb
vim ~/.ssh/ec2_host.erb

echo "Generating SSH config..."
profile_list=$(grep "\[" ~/.aws/credentials | tr -d '[]' | paste -sd ',' -)
/ec2-ssh-config-generator/generate-config.rb "${profile_list}" > ~/.ssh/config

echo "To regenerate: \`/ec2-ssh-config-generator/generate-config.rb ${profile_list} > ~/.ssh/config\`"

echo "Type 'ssh **' then TAB to start!"

bash
