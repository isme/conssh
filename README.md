# Conssh

![A drawing of a conch shell](Conch_drawing.jpg)

Conssh is a utility to configure SSH. It generates config files with connection
information for a fleet of EC2 instances.

Use it together with an autocompletion tool that understands SSH, like
[bash-completion] or [fzf].

Basic usage:

```
$ AWS_REGION=us-west-1 AWS_PROFILE=staging bundle exec conssh > ~/.ssh/config
$ ssh …
```

[bash-completion]: https://github.com/scop/bash-completion
[fzf]: https://github.com/junegunn/fzf

## Background

It is inspired by similar tools such as [ec2ssh] and [aws-ssh-config].

This is experimental code. See the issues section for the work to make it more
robust.

Notable features:

* Written in [Ruby] using version 3 of the [AWS SDK]
* Unit tested using the [Moto] mock AWS API server
* Packaged as a Ruby gem to use as a library or an executable

[ec2ssh]: https://github.com/mirakui/ec2ssh
[aws-ssh-config]: https://github.com/gianlucaborello/aws-ssh-config
[Ruby]: https://www.ruby-lang.org/
[AWS SDK]: https://github.com/aws/aws-sdk-ruby/
[Moto]: https://github.com/spulec/moto

## How do I get set up?

See the [setup](setup.md) document for an early version step-by-step setup.

## Demo

Build the docker container and follow the instructions when the container runs.

```bash
cd demo
docker build --tag demo .
docker run -it demo
```
