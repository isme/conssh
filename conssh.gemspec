
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "conssh/version"

Gem::Specification.new do |spec|
  spec.name          = "conssh"
  spec.version       = Conssh::VERSION
  spec.authors       = ["Iain Samuel McLean Elder"]
  spec.email         = ["iainelder@hotmail.co.uk"]

  spec.summary       = %q{Configure SSH to connect to an EC2 fleet}
  spec.description   = <<~DESCRIPTION
    Conssh is a utility to configure SSH. It generates config files with
    connection information for a fleet of EC2 instances. Use it together with
    an autocompletion tool that understands SSH, like bash-completion or fzf.
  DESCRIPTION
  spec.homepage      = "https://bitbucket.org/isme/conssh"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rubocop", "~> 0.54"

  spec.add_dependency "aws-sdk-ec2", "~> 1.0"
end
