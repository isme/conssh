# frozen_string_literal: true

require 'conssh/inventory'
require 'aws-sdk-ec2'

RSpec.describe Conssh::Inventory do
  before(:each) do
    moto_server = ENV['MOTO_SERVER'] || 'moto_server'
    @moto_pid = spawn(moto_server, '-p5000', 'ec2', :err => '/dev/null')
  end

  after(:each) do
    Process.kill('TERM', @moto_pid)
    Process.wait(@moto_pid)
  end

  let(:client) do
    Aws::EC2::Client.new(
      :endpoint => 'http://localhost:5000',
      :region => 'eu-west-1',
      :access_key_id => 'TEST',
      :secret_access_key => 'TEST'
    )
  end
  let(:resource) { Aws::EC2::Resource.new(:client => client) }
  let(:inv) { Conssh::Inventory.new(:client => client) }

  it 'returns empty list when there are no instances' do
    expect(inv.hosts).to eq([])
  end

  it 'returns a instance' do
    expected = resource.
               create_instances(:min_count => 1, :max_count => 1).
               to_a.first

    actual = inv.hosts.first

    expect(actual.instance_id).to eq(expected.instance_id)
  end
end
