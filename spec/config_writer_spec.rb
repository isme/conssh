# frozen_string_literal: true

require 'conssh/config_writer'

RSpec.describe Conssh::ConfigWriter do
  let(:out) { StringIO.new }
  let(:r) { Conssh::ConfigWriter.new(:out => out) }

  let(:inst1) do
    instance_double(
      'Aws::EC2::Instance',
      :id => 'i-12345678',
      :public_ip_address => '23.45.67.89'
    )
  end

  let(:inst2) do
    instance_double(
      'Aws::EC2::Instance',
      :id => 'i-23456789',
      :public_ip_address => '34.56.78.90'
    )
  end

  let(:inst3) do
    instance_double(
      'Aws::EC2::Instance',
      :id => 'i-34567890',
      :public_ip_address => '45.67.89.01'
    )
  end

  it 'writes the instance name and public IP' do
    conf = <<~SSH_CONFIG
      Host i-12345678
      HostName 23.45.67.89
    SSH_CONFIG
    r.render(inst1)
    expect(out.string).to eq(conf)
  end

  it 'writes multiple instances' do
    conf = <<~SSH_CONFIG
      Host i-12345678
      HostName 23.45.67.89

      Host i-23456789
      HostName 34.56.78.90

      Host i-34567890
      HostName 45.67.89.01
    SSH_CONFIG
    r.render([inst1, inst2, inst3])
    expect(out.string).to eq(conf)
  end

  it 'writes nothing when input is empty' do
    r.render([])
    expect(out.string).to eq('')
  end

  it 'raises ArgumentError without instance arg' do
    expect { r.render }.to raise_error(ArgumentError)
  end
end
