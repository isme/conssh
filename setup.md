I assume you have just one SSH keypair to access all of your EC2 instances, and you keep it locally.

I also assume you need to access instances in multiple accounts and multiple regions.

I assume you have docker installed and ready to use in your environment.

Before we start, get the all the credentials required to access the API of your AWS account and the SSH sessions of your EC2 instances.

The brackets here are just a fancy way to generate filenames with a common prefix. Saves a bit of typing.

```
less ~/.aws/credentials ~/.ssh/id_rsa{,.pub}
```

Start a new Ubuntu container.

```
docker run -it ubuntu:16.04
```

A fresh Ubuntu image doesn't have a package list, so we need to update it first before any of the install commands will work.

```
apt-get update
```

The image doesn't have a bunch of things that you will expect to be there when you setting up and playing around. I assume you know what these are, let's just get them installed!

```
apt-get install vim less man wget git
```

The bash-completion package is also expected, and deserves a special mention here because it's what makes the magic happen later when have the correct SSH config.

Change the default bindings to make the autocomplete behavior a bit nicer in bash.

To make it take effect in the existing session we have to source the global profile again.

```
apt-get install bash-completion
bind "TAB:menu-complete"
bind "set show-all-if-ambiguous on"
source /etc/profile
```

The image doesn't have an SSH client either, so let's install that.

We will also quickly set up the usual file with the right permissions so that ssh won't complain.

See way below for how to install a later version of the client that supports the Include directive.

```
apt-get install openssh-client
mkdir ~/.ssh/
touch ~/.ssh/{known_hosts,config,id_rsa,id_rsa.pub}
chmod --recursive 0600 ~/.ssh/
```

While we are here, we can configure it with our SSH keys we will use to access the instances.

```
vim ~/.ssh/id_rsa
vim ~/.ssh/id_rsa.pub
```

Installing AWS CLI is not strictly necessary, but it is the easiest way to configure the AWS profiles.

Paste access key and secret key from aws credentials.

Check it works by querying your user info.

```
apt-get install awscli
aws configure --profile staging
aws iam get-user --profile staging
aws configure --profile production
aws iam get-user --profile production
```

As ec2ssh is a Ruby gem, we first have to install Ruby so that we can use gem to install it.

It unfortunately depends on the Nokogiri gem, so we have to install a bunch of other non-Ruby stuff to make it work.

```
apt-get install ruby
apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev
gem install ec2ssh
```

Run init and check that an empty block is created and that the default config is set up.

```
ec2ssh init
cat ~/.ssh/config
cat ~/.ec2ssh
```

In the default config you'll see sections for sets of credentials and a list of regions.

First let's set up the tool to query all regions so we are sure the list is complete.

```
aws ec2 describe-regions --region us-east-1 --query 'Regions[].RegionName' --output text
```

Paste the output of that command into the region directive (there is a commented-out example).

```
vim ~/.ec2ssh
```

The list is static, so you will have to update it when Amazon adds a new region. Thankfully here that happens relatvively infrequently.

Unfortunately the program queries each region serially, so you might have a to wait a while. It would be great to do the queries in parallel, but it probably requires modifying the program.

If you don't mind having a copy of your credentials in the ec2ssh config, then set up a staging and a production section with your keys.

Otherwise see the next section for how to generate an equivalent ssh config using just the prodi

If no credentials are set in the config, it will use whatever profile is set in the environment.

If you prefer to manage all your AWS credentials as CLI profiles, then you simulate the behavior with a few extra steps.

It would be awesome if the program was profile aware, but that probably requires modifying the program.

```
ec2ssh init --path ~/.ssh/config_staging
AWS_PROFILE=staging ec2ssh update --path ~/.ssh/config_staging
ec2ssh init --path ~/.ssh/config_production
AWS_PROFILE=production ec2ssh update --path ~/.ssh/config_production
```

As of SSH 7.3 (Mac OS X Sierra has 7.4) , you can include these compartmentalizied configrations using the new Include directive.

However, bash and zsh still don't support completion for included configs (although the feature appears to be on its way for zsh).

For now, the easiest thing is just to combine the two configs together into one file.

This also has the advantage of working with older versions of SSH that don't support the Include directive (Ubuntu Xenial has 7.2).

```
cat ~/.ssh/config_{staging,production} > ~/.ssh/config
```

Edit the config how we see fit.

```
vim ~/.ec2ssh # add public_ip_address
ec2ssh update
````

Bunch of commands to install ssh 7.4, part of ubuntu's latest stable release (Zesty Zapus) and the same version on Mac OS X.

```
wget https://launchpad.net/ubuntu/+source/openssh/1:7.4p1-10/+build/12390736/+files/openssh-client_7.4p1-10_amd64.deb
dpkg -i openssh-client_7.4p1-10_amd64.deb 
wget https://launchpad.net/ubuntu/+source/openssh/1:7.4p1-10/+build/12390736/+files/ssh-krb5_7.4p1-10_all.deb
dpkg -i ssh-krb5_7.4p1-10_all.deb 
wget https://launchpad.net/ubuntu/zesty/amd64/libgssapi-krb5-2/1.15-1
wget http://launchpadlibrarian.net/296470446/libgssapi-krb5-2_1.15-1_amd64.deb
dpkg -i libgssapi-krb5-2_1.15-1_amd64.deb 
wget http://launchpadlibrarian.net/296470457/libkrb5support0_1.15-1_amd64.deb
dpkg -i libkrb5support0_1.15-1_amd64.deb 
dpkg -i libgssapi-krb5-2_1.15-1_amd64.deb 
wget http://launchpadlibrarian.net/296470455/libkrb5-3_1.15-1_amd64.deb
deb -i libkrb5-3_1.15-1_amd64.deb 
dbkg -i libkrb5-3_1.15-1_amd64.deb 
dpkg -i libkrb5-3_1.15-1_amd64.deb 
apt-get upgrade
wget https://launchpad.net/ubuntu/+source/krb5/1.15-1/+build/11519847/+files/libk5crypto3_1.15-1_amd64.deb
dpkg -i libk5crypto3_1.15-1_amd64.deb 
deb -i libkrb5-3_1.15-1_amd64.deb 
dpkg -i libkrb5-3_1.15-1_amd64.deb 
ls *.deb
dpkg -i openssh-client_7.4p1-10_amd64.deb
dpkg -i libgssapi-krb5-2_1.15-1_amd64.deb
dpkg -i openssh-client_7.4p1-10_amd64.deb
```
